# Aldi's Bet

Aldi's Bet is a short quest mod featuring Aldi, an otherwise unremarkable Nord who has been living in Khuul since 2002.
Aldi has a dream: she wants to open up a store. She's also made a bet with her cousin. Maybe she'll be able to open a store if you help her win that bet.

## Installing the mod

Use a mod manager, use the OpenMW Launcher, or just merge the Data Files directory contained in the .7z file into Morrowind's Data Files directory. Your choice.
Once you've done that, activate EE_Aldi.ESP.
