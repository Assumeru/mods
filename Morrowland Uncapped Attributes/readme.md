# Morrowland Uncapped Attributes

An experience based levelling mod for OpenMW 0.49.

Back in the early 2000s there was [From Hell to Heaven, Morrowland](https://archive.org/details/morrowland) by GameBro and Berzerk.
Morrowland is an ambitious mod adding a large landmass east of Vvardenfell. A landmass rather too big for two people to adequately fill with content.
Most of Morrowland's worldspace is endless wilderness filled with monsters to fight and little else. Aside from this and a number of quests,
Morrowland also replaces Morrowind's levelling system. This mod is a reimplementation of that levelling system with the minor change of uncapping skills and attributes.

## How it works

Every time something dies in your vicinity, you receive experience points based on its stats.
Using your skills will no longer improve them. Increasing your major and minor skills using trainers or skill books is possible, but won't count towards your next level.
Instead you will gain levels by sleeping after acquiring enough experience points.

When you level up, you will have to divide 12 skill increases across skills of your choice with a maximum of 3 points per skill.
Any 2 skill points assigned to skills with the same governing attribute will increase that attribute by 1.
Aside from these natural attribute increases, you can also increase 3 attributes of your choice by 1 point each.

### Formulae

Experience gain for creatures: `[base health] / 2` rounded down. If a creature has less than 2 health, `5 * [level]` is used instead.

Experience gain for NPCs: `[level] * 5 + 15`.

Additional experience needed to reach the next level: `100 * ([current level] + 1)`.

Total experience needed to reach the next level: `50 * [current level]^2 + 150 * [current level]`.

## Installing the mod

1. Unpack the .7z file in a directory of your choosing.
2. Go to the **Data Directories** tab under **Data Files** in the OpenMW Launcher.
3. Use the **Append** button to add the **00 Core** directory from the .7z file.
4. Activate **EE_MLua.omwaddon** in the **Content Files** tab.

This mod can be played using an existing character.

## Uninstalling the mod

1. Go to the **Content Files** tab under **Data Files** in the OpenMW Launcher.
2. Uncheck **EE_MLua.omwaddon**.
3. (Optional) Delete the mod's files.
