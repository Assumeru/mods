'use strict';

const fs = require('fs');
const child_process = require('child_process');

const build = __dirname + '/build';
const core = build + '/00 Core';
const src = __dirname + '/src';

const FLAGS_BY_NAME = {
	GLOBAL: 1,
	CUSTOM: 2,
	PLAYER: 4
};
const TYPES_BY_NAME = {
	CREATURE: 1095062083,
	NPC: 1598246990
};

fs.rmSync(build, { force: true, recursive: true });
fs.mkdirSync(build);
fs.mkdirSync(core);

const files = fs.readdirSync(src, { withFileTypes: true });

for(const file of files) {
	if(file.isDirectory()) {
		fs.cpSync(file.path + '/' + file.name, core + '/' + file.name, { recursive: true });
	}
}

fs.cpSync(__dirname + '/readme.md', core + '/readme.md');
const addon = core + '/EE_MLua.omwaddon';
fs.cpSync(src + '/EE_MLua.ESP', addon);

fs.readFileSync(src + '/EE_MLua.omwscripts', { encoding: 'utf8' }).split('\n').forEach(line => {
	line = line.trim();
	if(line === '' || line[0] === '#') {
		return;
	}
	const colon = line.indexOf(':');
	const tags = line.slice(0, colon).trim();
	const path = line.slice(colon + 1).trim();
	let flags = 0;
	const types = [];
	tags.split(',').forEach(tag => {
		tag = tag.trim();
		if(tag === '') {
			return;
		}
		if(tag in FLAGS_BY_NAME) {
			flags |= FLAGS_BY_NAME[tag];
		} else if(tag in TYPES_BY_NAME) {
			types.push(TYPES_BY_NAME[tag]);
		} else {
			throw new Error(`Unknown tag: ${tag}`);
		}
	});
	const luasSize = path.length + 1;
	const luafSize = 4 + 4 * types.length;
	const lualSize = luasSize + luafSize + 16;
	const buffer = Buffer.alloc(lualSize + 16);
	let offset = 0;
	offset += buffer.write('LUAL', offset);
	offset = buffer.writeInt32LE(lualSize, offset) + 8;
	offset += buffer.write('LUAS', offset);
	offset = buffer.writeInt32LE(luasSize, offset);
	offset += buffer.write(path, offset) + 1;
	offset += buffer.write('LUAF', offset);
	offset = buffer.writeInt32LE(luafSize, offset);
	offset = buffer.writeInt32LE(flags, offset);
	for(const type of types) {
		offset = buffer.writeInt32LE(type, offset);
	}
	fs.appendFileSync(addon, buffer);
});

child_process.spawnSync('7z', ['a', '-mmt4', '-mx9', build + '/EE_MLua.7z', core]);

